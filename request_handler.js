/**
 * request_handler.js
 */

var fs = require('fs');

var responder = require('./responder');

function foo(data, response) {
    console.log("Request handler ´foo´ was called.");

    var jsonResponse = {
        "trackinfo":{
            "parcelnr":123456,
            "provider":"DHL",
            "trackdetails":{
                "trackdetail":[
                    {
                        "date":"01.01.2010",
                        "info":"Got parcel from Customer"
                    },
                    {
                        "date":"02.01.2010",
                        "info":"Shipped to Target depot"
                    },
                    {
                        "date":"03.01.2010",
                        "info":"Delivered to Customer"
                    }
                ]
            }
        }
    };

    responder.respond(response, 200, {
        'headers': { 'Content-Type': 'application/json' },
        'content': JSON.stringify(jsonResponse)
    });

}

function bar(data, response) {
    console.log("Request handler ´bar´ was called.");

    /**
     *
     * Nej! As the saying goes:
     *   ´´In node, everything runs in parallel, except your code´´
     *
     * (function() {
     *     var startTime = new Date().getTime();
     *     while (new Date().getTime() < startTime + 1e4);
     * })();
     *
     */

    setTimeout(function() {
        responder.respond(response, 200, {'content': "Hello Bar" });
    }, 1e4);
}

exports.foo = foo;
exports.bar = bar;
