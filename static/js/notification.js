/**
 * notification.js
 */

var Notification = (function() {

    // Konfiguration – minsta intervall mellan meddelanden (ms)
    var MIN_RENDER_INTERVALL = 5e2;

    // Konfiguration – max antal samtidiga statusmeddelanden + max kölängd
    var MAX_CONCURRENT = 3;
    var MAX_QUEUED = 1e2;

    // Konfiguration – default-värden för nya meddelanden
    var DEFAULTS = {
        message: '',
        color: '#333',
        backgroundColor: '#ccc',
        removeButton: true,
        timeout: 6e3,
        onRenderCallback: function() {
            console.debug('Notification rendered.');
        },
        onCloseCallback: function() {
            console.debug('Notification closed.');
        }
    };

    // Klassvariabler
    var wrapperSelector = '.notification-view';
    var wrapperEl;
    var lastRenderTimestamp;
    var concurrentNotifications = 0;
    var notificationQueue = [];

    // Funktion som genererar ett element för vårt meddelande
    function generateNotification(label, message, color, backgroundColor, removeButton) {
        var wrapperDiv = document.createElement('div');
        var labelSpan = document.createElement('span');
        var messageSpan = document.createElement('span');

        wrapperDiv.style.backgroundColor = backgroundColor;

        labelSpan.style.fontWeight = 700;
        labelSpan.style.color = color;
        labelSpan.innerHTML = label;
        wrapperDiv.appendChild(labelSpan);

        messageSpan.style.color = color;
        messageSpan.innerHTML = message;
        wrapperDiv.appendChild(messageSpan);

        if (removeButton) {
            removeButton = document.createElement('button');
            removeButton.innerText = 'X';
            removeButton.onclick = function() {
                fireEvent.call(wrapperDiv, 'notificationremoved', { });
                wrapperEl.removeChild(wrapperDiv);
            };
            wrapperDiv.appendChild(removeButton);
        }

        return wrapperDiv;
    }

    // Funktion som binder variabeln ´´wrapperEl´´ till vår ´´notificationview´´-sektion.
    function bindNotificationView(callback) {
        wrapperEl = document.querySelector(wrapperSelector);
        if (wrapperEl) {
            return callback();
        } else {
            document.addEventListener('readystatechange', function () {
                if (document.readyState === "complete") {
                    callback();
                }
            });
        }
    }

    // Ansvarar för att rendera meddelandet i DOM:en och anropa callback.
    function renderNotification(notification, timeout, onRenderCallback, onCloseCallback) {

        if (concurrentNotifications == MAX_CONCURRENT) {
            notificationQueue.push(function() {
                renderNotification(notification, timeout, onRenderCallback, onCloseCallback);
            });
            return;
        }

        var renderDelay = 0;
        var timeoutHandle;

        // Ser till att vi inte renderar meddelanden snabbare än ´´MIN_RENDER_INTERVALL´´
        if (lastRenderTimestamp) {
            //console.debug('old lastRenderTimestamp: '+lastRenderTimestamp);
            var now = new Date();
            if (now <= lastRenderTimestamp) {
                lastRenderTimestamp = new Date(lastRenderTimestamp.valueOf() + MIN_RENDER_INTERVALL);
                renderDelay = lastRenderTimestamp - now;
            } else {
                renderDelay = Math.abs(lastRenderTimestamp - now);
                //console.debug('dt(renderDelay): '+renderDelay);
                renderDelay = renderDelay < MIN_RENDER_INTERVALL ? MIN_RENDER_INTERVALL - renderDelay : 0;
                lastRenderTimestamp = new Date(now.valueOf() + renderDelay);
            }

            //console.debug('updated renderDelay: '+renderDelay);

            //console.debug('new lastRenderTimestamp 1: '+lastRenderTimestamp);

        } else {
            lastRenderTimestamp = new Date();
            //console.debug('new lastRenderTimestamp 2: '+lastRenderTimestamp);
        }

        concurrentNotifications++;

        setTimeout(function() {

            // tar bort vårt meddelande efter ´´timeout´´ ms
            if (timeout) {
                timeoutHandle = setTimeout(function() {
                    wrapperEl.removeChild(notification);
                    concurrentNotifications--;
                    if (concurrentNotifications < MAX_CONCURRENT) {
                        fireEvent.call(Notification, 'notificationslotopen', {});
                    }
                    onCloseCallback();
                }, timeout);
            }

            // rensar upp automatisk timeout ifall användaren klickar bort meddelandet
            notification.addEventListener('notificationremoved', function() {
                if (timeoutHandle) {
                    clearTimeout(timeoutHandle);
                    concurrentNotifications--;
                    if (concurrentNotifications < MAX_CONCURRENT) {
                        fireEvent.call(Notification, 'notificationslotopen', {});
                    }
                    onCloseCallback();
                }
            });

            //console.debug('Notification rendered (delay: ' + renderDelay + ' ms).');
            wrapperEl.appendChild(notification);
            onRenderCallback();

        }, renderDelay);
    }

    this.addEventListener('notificationslotopen', function() {
        if (notificationQueue.length) {
            notificationQueue.shift()();
        }
    });

    console.debug(new Date() + ': Notification prototype initialized.');

    // Vårt publika gränssnitt, tilldelas ´´Notification´´-objektet i vårt globala scope.
    return function(options, callback) {
        var label = options.label || DEFAULTS.label;
        var message = options.message || DEFAULTS.message;
        var color = options.color || DEFAULTS.color;
        var backgroundColor = options.backgroundColor || DEFAULTS.backgroundColor;

        var removeButton = DEFAULTS.removeButton;
        if (typeof options.removeButton === 'boolean') {
            removeButton = options.removeButton;
        }

        var timeout = DEFAULTS.timeout;
        if (typeof options.timeout === 'number') {
            timeout = options.timeout;
        }

        var onCloseCallback = options.onCloseCallback || DEFAULTS.onCloseCallback;
        var onRenderCallback = options.onRenderCallback || DEFAULTS.onRenderCallback;

        var notification = generateNotification(label, message, color, backgroundColor, removeButton);

        if (notificationQueue.length >= MAX_QUEUED) {
            throw new Error('Notification queue max length reached (' + notificationQueue.length + '/' + MAX_QUEUED + ')');
        }

        if (!wrapperEl) {
            bindNotificationView(function() {
                renderNotification(notification, timeout, onRenderCallback, onCloseCallback);
            });
        } else {
            renderNotification(notification, timeout, onRenderCallback, onCloseCallback);
        }

        return this;
    };

})();


