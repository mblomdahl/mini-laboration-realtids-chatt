/**
 * utility.js
 */

var crypto = require('crypto');
var responder = require('../responder');

function generateRandStr(length, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randStr = '';
    for (var charSetLength = charSet.length; --length; ) {
        var randIndex = Math.floor(Math.random() * charSetLength);
        randStr += charSet[randIndex];
    }
    return randStr;
}

function createMessageHash(UUID, message) {
    var HMAC = crypto.createHmac('md5', UUID);
    HMAC.update(message);

    return HMAC.digest('base64');
}

function wrapErrorResponse(err) {
    var error = err.message.split('::');
    return {
        meta: {

        },
        status: parseInt(error[0]),
        data: [],
        errors: [ error[1] ]
    };
}

function wrapDataResponse(data) {
    return {
        meta: {

        },
        status: 200,
        data: data,
        errors: []
    };
}

exports.createMessageHash = createMessageHash;
exports.generateRandStr = generateRandStr;
exports.wrapErrorResponse = wrapErrorResponse;
exports.wrapDataResponse = wrapDataResponse;
