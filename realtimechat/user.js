/**
 * user.js
 */

var utility = require('./utility');
var message = require('./message');

var DISCONNECTED = 0;
var CONNECTED = 1;
var INACTIVE = 2;
var ACTIVE = 3;

var UserStore = new function() {

    // Default ´´userDetails´´ configuration
    var DEFAULT = {
        username: undefined,
        UUID: undefined,
        socketID: undefined,
        state: CONNECTED,
        lastMessageHash: undefined,
        timeStamps: {
            lastInteraction: undefined,
            firstConnected: undefined,
            lastConnected: undefined,
            disconnected: undefined
        }
    };

    var users = {};

    // Private helper
    function generateUUID() {
        var UUID = utility.generateRandStr(8);
        while (users[UUID]) {
            UUID = utility.generateRandStr(8);
        }
        return UUID;
    }

    // Private
    function verifyMessages(UUID, username, connected, messages) {
        messages.forEach(function(message) {
            if (false && message.UUID !== UUID || message.username !== username) {
                console.log('real username:'+username);
                console.log('real UUID:'+UUID);
                throw new Error("403::Bad Request (Forged identity)");
            }
            if (message.timeStamp < connected) {
                throw new Error("403::Bad Request (Forged timestamp)");
            }
        });
    }

    // Private
    function verifyUser(socketID, userDetails) {
        // TODO(mats.blomdahl@gmail.com): Verify username validity.
        if (!(userDetails.UUID && userDetails.username && userDetails.socketID)) {
            throw new Error("400::Bad Request (Identifiers missing)");
        }

        var chatUser = users[userDetails.UUID];
        if (!chatUser) {
            throw new Error("404::Not Found (Unidentified UUID)")
        }
        if (!(chatUser.getSocketID() === socketID && chatUser.getUsername() === userDetails.username)) {
            throw new Error("403::Forbidden (Forged credentials)");
        }
        if (!chatUser.getState()) {
            throw new Error("401::Unauthorized (Disconnected)");
        }

        return chatUser;
    }

    // Private
    function createUser(config) {
        var userDetails = {
            username: config.username || DEFAULT.username,
            UUID: config.UUID || generateUUID(),
            socketID: config.socketID || DEFAULT.socketID,
            state: config.state || DEFAULT.state,
            lastMessageHash: config.lastMessageHash || DEFAULT.lastMessageHash
        };

        var userMetaData = {
            timeStamps: DEFAULT.timeStamps
        };

        userMetaData.timeStamps.lastConnected = userMetaData.timeStamps.firstConnected = userMetaData.timeStamps.lastInteraction = new Date().valueOf();

        if (config.timeStamps) {
            for (var timeStamp in config.timeStamps) {
                userMetaData.timeStamps[timeStamp] = config.timeStamps[timeStamp];
            }
        }

        // Public instance method
        this.getMessageHistory = function() {
            var messages = message.get(userDetails.UUID, userDetails.lastMessageHash);
            if (messages.length) {
                userDetails.lastMessageHash = messages[messages.length - 1].hash;
            }
            return messages;
        };

        // Public instance method
        this.getPublicDetails = function() {
            var publicDetails = {
                username: userDetails.username,
                UUID: userDetails.UUID,
                state: userDetails.state
            };
            return publicDetails;
        };

        // Public instance method
        this.getFullDetails = function() {
            return userDetails;
        };

        // Public instance method
        this.getMetaData = function() {
            return userMetaData;
        };

        // Public instance method
        this.getUUID = function() {
            return userDetails.UUID;
        };

        // Public instance method
        this.getUsername = function() {
            return userDetails.username;
        };

        // Public instance method
        this.getSocketID = function() {
            return userDetails.socketID;
        };

        // Public instance method
        this.getState = function() {
            return userDetails.state;
        };

        // Public instance method
        this.setState = function(state) {
            userMetaData.timeStamps.lastInteraction = new Date().valueOf();
            userDetails.state = state;
            return this;
        };

        // Public instance method
        this.addMessages = function(messages) {
            var firstConnected = userMetaData.timeStamps.firstConnected;
            var UUID = userDetails.UUID;
            var username = userDetails.username;
            verifyMessages(UUID, username, firstConnected, messages);
            return message.add(messages);
        };

        // Public instance method
        this.refreshLastInteraction = function() {
            userMetaData.timeStamps.lastInteraction = new Date().valueOf();
            return this;
        };

        return this;
    }

    // Public interface
    this.getUser = function(socketID, userDetails) {
        if (socketID && userDetails) {
            return verifyUser(socketID, userDetails).refreshLastInteraction();

        } else {
            if (socketID) {
                for (var UUID in users) {
                    if (users[UUID].socketID == socketID) {
                        return users[UUID];
                    }
                }
                return;

            } else {
                throw new Error("400::Bad Request (Identifiers missing)");
            }
        }
    };

    // Public interface
    this.listUsers = function() {
        var userList = [];
        for (var UUID in users) {
            userList.push(users[UUID].getFullDetails());
        }
        return userList;
    };

    // Public interface
    this.loginUser = function(socketID, userDetails) {
        if (!userDetails.username) {
            throw new Error("400::Bad Request (Identifiers missing)");
        }

        // TODO(mats.blomdahl@gmail.com): Verify username validity.
        var user = new createUser({
            username: userDetails.username,
            UUID: userDetails.UUID,
            socketID: socketID
        });
        users[user.getUUID()] = user;
        return user;
    };

    // Public interface
    this.logoutUser = function(socketID, userDetails) {
        return verifyUser(socketID, userDetails).setState(DISCONNECTED).refreshLastInteraction();
    };

};

exports.login = UserStore.loginUser;
exports.get = UserStore.getUser;
exports.list = UserStore.listUsers;
exports.logout = UserStore.logoutUser;
