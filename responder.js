/**
 * responder.js
 */

function respond(response, status, eOpts) {

    var headers = eOpts['headers'] || {'Content-Type': 'text/plain'};

    response.writeHead(status, headers);

    if (eOpts['content']) {
        response.write(eOpts['content']);
    } else {
        if (status == 404) {
            response.write("404 Not Found");
        }
    }

    response.end();

}

exports.respond = respond;
