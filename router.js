/**
 * router.js
 */

var path = require('path');
var fs = require('fs');

var request_handler = require('./request_handler');
var responder = require('./responder');

var STATIC_DIR_NAME = path.join(__dirname, 'static');

var MIME_TYPES = {
    '.appcache': 'text/cache-manifest',
    '.html': 'text/html',
    '.js': 'application/javascript',
    '.css': 'text/css',
    '.gif': 'image/gif',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.svg': 'image/svg+xml',
    '.swf': 'application/x-shockwave-flash'
};

function route(handle, pathname, data, response) {
    console.log("Route initiated for " + pathname);
    if (typeof handle[pathname] === 'function') {
        handle[pathname](data, response);

    } else {
        var staticFilepath = path.join(STATIC_DIR_NAME, path.normalize(pathname));

        /**
         * Validations inspired by http://docs.nodejitsu.com/articles/file-system/security/introduction
         */
        if (pathname.indexOf('\0') !== -1) {
            responder.respond(response, 400, { content: 'That was evil.' });
            return;
        }
        if (staticFilepath.indexOf(STATIC_DIR_NAME) !== 0) {
            responder.respond(response, 400, { content: 'Trying to sneak out of the web root?' });
            return;
        }

        fs.readFile(staticFilepath, function(error, file) {
            if (error) {
                responder.respond(response, 404, { });
                console.error("Static resource " + staticFilepath + " unresolved.");
            } else {
                var fileExtname = path.extname(staticFilepath).toLowerCase();
                var httpHeaders = {};
                httpHeaders['Content-Type'] = MIME_TYPES[fileExtname] || 'text/plain';

                responder.respond(response, 200, { 'headers': httpHeaders, 'content': file });
                console.log("Static resource " + staticFilepath + " served.");
            }
        });

    }
    console.log("Routing completed for " + pathname);
}

exports.route = route;
